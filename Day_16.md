# Day 16

**Day 16 - 12th** **April.**

On day 16, there was a session on User Interface (UI), User Experience (UX) and Wire Frame (WF). As I have explained about UI, UX and WF in the previous daily articles, I am explaining in this essay only the interaction I had with Haridev about Security Team. when I spoke to Saravana about Security operations he gave me a set of questionnaires to to know about and he also asked me to discuss with Haridev on the topics. I discussed with Haridev regarding Security Operations. 

Why are Security Operations required?

Security Operations maintain an active 24 hrs. mechanisms to protect the organizations and their products from active threats. Their tools and mechanisms stop the external and internal threats from attacking the networks of the organizations. They help protect the user and the service provider from active threats. This makes security operations an essential component in any organization. More users than ever before rely on mobile applications for a majority of their digital tasks over traditional desktop applications. All popular mobile platforms provide security controls designed to help software developers build secure applications. 

Common issues that affect mobile apps include:

- Storing or unintentionally leaking sensitive data in ways that it could be read by other applications on the user’s phone.
- Implementing poor authentication and authorization checks that could be bypassed by malicious applications or users.
- Using data encryption methods that are known to be vulnerable or can be easily broken.
- Transmitting sensitive data without encryption over the Internet.

Saravana explained with an example of WhatsApp’s encryption to explain security operations. The security operation helps to test whether the messages are encrypted. This helps in data leak

How are Security operations working?

Security operations can proactively secure the company from external threats through the identification and analysis of everything that is going on in the environment. The main responsibility of the Security operation is to prevent and detect threats, Investigate suspicious activities, Implementing a response. Security engineers are responsible for detecting and responding to security threats, the security analyst implements security measures and takes charge of disaster recovery plans. The security engineer is responsible for maintaining and updating tools and systems and documenting and disseminating security protocols to other employees. Haridev also explained that they are looking for vulnerabilities in the app itself,  testing also looks for issues in the back-end services that are used by the application. By focusing both on the app and its back-end services, ensuring all aspects of the application are covered during testing.

What is cybersecurity?

Cybersecurity is the practice of protecting systems, networks, and programs from digital attacks. These cyber-attacks are usually aimed at accessing, changing, or destroying sensitive information; extorting money from users; or interrupting normal business processes.

What are the security attacks that can happen in applications?

Social Engineering, Data Leakage via Malicious Apps, Unsecured Public WiFi, End-to-End Encryption Gaps, Internet of Things (IoT) Devices, Spyware, Poor Password Habits, Lost or Stolen Mobile Devices.