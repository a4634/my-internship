# Day 11

**Day 11 - 5th** **April.**

The 11th day of the internship. The first half was interesting, I was searching for answers for my algorithm questions. My mentor asked me to teach the computer, the basic operations like addition, subtraction, multiplication and division. He told me to consider the computer dump. This made me think deeper about the computer. It pushed me think the origin of invention of the computers. I[t](http://computers.It) made me think how the first computer would have been programmed. I had conversations with the people in Surfboard to find a solution. I had healthy arguments on this topic. They gave me insights and clues to answer my algorithm problems. 

In the second half we had presentation on networking by Arun. It was a lengthy presentation wherein he covered three big topics. The presentation consist of 27 slides.

- Computer networking and its types
- Network Topology and hardware component
- Computer Hardware and types of computer hardware

**Computer networking and its types**

The computer network is an interconnection of two or more computers that are able to exchange information. The computer may be connected via any data communication link such as Ethernet, cables etc. They may located in room, building or any where in  the world. There are three types of network LAN, MAN, WAN. LAN covers a small region of space, typically a single building. It is the smallest and fastest network compared to MAN and WAN. MAN is Metropolitan Area Network. It is a collection  of LANS with the same geographical area for instance a city. Wide Area Network (WAN) is the largest network of all network types. The internet is the largest WAN in the world. It covers States, Country or Continent.

**Network Topology and hardware component**

Network Topology reefers to the physical or logical layout of a network. It defines the way how different nodes are placed and interconnected with each other. It also explains how the data is transferred between these nodes. The fundamental shapes are Bus, Ring, Star, Mesh. 

Bus topology - It consist of a single cable called the back bone, connecting all nodes. 

Ring topology - Each node is connected to the nearest node so that the entire network forms a circle. 

Star topology - Every node on the network is connected through a central device called hub or switch.

Mesh topology - Each computer is connected to every other computer. As the name suggests its forms a mesh. 

Hub, Repeater, Bridge, Router, Gateway are some of the Hardware components.

Hub is a multiport connecting device that is used to interconnect LAN devices. It is used to extend the physical length of the network.

Repeater is a device which amplifies or boosts the signal before passes through the next section of the cable.

Bridges, as the name suggests it passes and receive data from one LAN to another.

Router is a device that connects multiple networks using similar or different protocols. It is used to connect several networks.

Gateway receives data from one network and converts it according to the protocol of the other network.

**Computer Hardware and types of computer hardware**

Computer hardware is the physical component that a computer system requires to function. It encompasses everything with a circuit board that operates within a PC or Laptop. It includes the motherboard, Graphics Card, CPU, Ventilation fans, Webcam, Power Supply, Hard Disk, Mouse , Keyboard etc.