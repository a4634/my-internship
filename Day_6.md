# Day 6

**Day 6 - 29th March.** 

The day 6 of internship started with the topic of proportional statements. 

What is proportion?

When quantities have the same relative size. In other words they have the same ratio. Example: A rope's length and weight are in proportion.  

What is a proportional statement?

The statements that are by itself true or false are called proportional statements. Simply we can put it as true or false statements. Example: I parked my car outside. This statement may be true or may not be true. They can be also called as declaration or declarative statements. Kowshik asked for examples for proportional statements.

Proportional logic

Proportional logic can be applied to proportional statements i.e. the statements to which true or false can be assigned.

Variables and connections 

Connected variables will have the same value at any moment of time; i.e., changes of one variable will be immediately propagated to another variable declared as **dependent** one.

Logical not

Logical and

Logical or 

Various combinations proposition related to other proposition

Influence of one statement’s truth over the other statement’s truth

Statements that are always, sometimes, never true.

In propositional logic generally we use five connectives which are −

- OR (∨∨)
- AND (∧∧)
- Negation/ NOT (¬¬)
- Implication / if-then (→→)
- If and only if (⇔⇔).

HTML

HTML stands for Hyper Text Markup Language. We were given a introduction to HTML. We created a .html file using command line.

Example programming

<html>

<body>

<h1>My First Heading</h1>

<p>My first paragraph.</p>

</body>

</html>

Figma

One of the top websites for UI designers is Figma. Figma is used for mobile web designing, desktop web designing also used for other designing works. Figma is a professional website for UI design. Features of Figma 

- Design - UI screens can be designed using Figma
- Prototyping - Screen interaction  can be made clear using Figma
- Collaboration- The working file can be shared and collaborated with 2 team members in a easier manner for free version

Figma has both free version and premium. In free version two projects can be done at free of cost. 

Live editing can be done using Figma. It has a very simple and easier user interface. Figma desktop application is provided for mac and windows and it is not available for other OS.

Figma is a simple light weighted application. 

Frame tool gives different sizes of frame. For example, tablet, mobile, watches etc. Frame is the art board. In Figma art board is considered as shape. Frames can be changed in layer alone. User can lock the layer and also enable and disable visibility. Some of the shortcuts in Figma are

Hold shift for aspect rescaling

Ctrl+ zoom in

Ctrl- zoom out

Space + left mouse 

Move shortcut V

Alt +drag creates smart guides

Press shift and you can select multiple art boards