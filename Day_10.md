# Day 10

**Day 10 - 4th** April.

The 10th day of the internship started with my articles and assignments reviewed by Ameen. He gave me few more assignments to improve my knowledge in flutter. He explained how the front page is splitter into rows and columns. He also explained about the child, children concepts in flutter. He gave a task to take few application home page models and decode the page by segregating the page into rows and columns and also to write base code in flutter for each functions in the page.

The afternoon session was started by Kowshik. He explained the types of  Data, Segregation of Data, the various operators and their types

Data : Data can be simply put as Information about anything. It can also be explained as collection of information. The information may be letters, numbers, image etc. Data is always linked to something. If there is no link, the data may be useless. For example, our Aadhar card, PAN Card etc contain data. It is linked to the government where the data becomes useful.

Segregation of data or data classification: Data segregation or Data classification is used to easily group different information. For example files with extension .pdf. With the help of this we can easily identify the pdf files. 

Operators 

**Arithmetic Operators** are used to perform mathematical calculations.

- +
- -
- *
- /
- % remainder
- = assigning operator
- ++ increment [a++=a+1]

**Assignment Operators** are used to assign a value to a property or variable. Assignment Operators can be numeric, date, system, time, or text.

**Comparison Operators** are used to perform comparisons.

**Concatenation Operators** are used to combine strings.

**Logical Operators** are used to perform logical operations and include AND, OR, or NOT.

**Boolean Operators** include AND, OR, XOR, or NOT and can have one of two values, true or false.

Askin continued the session explaining algorithm. He helped us solve few problems. He explained how a problem should be broken into smaller steps.

I learned the difference between Test case and Test script in software testing.

**Test Case**

A test case is a document, which has a set of test data, preconditions, expected results and postconditions, developed for a particular test scenario in order to verify compliance against a specific requirement. The test engineer writes the test case. It contains input action and the expected outcome. Test case helps in Traceability, Requirements coverage and reusability. The test case document consist of  

• Title
• Description
• Prerequisite
• Test data
• Test Steps
• Expected Results

**Test Script**

Test Script is a set of chronological steps to validate a test case. Three ways to create test script are 1) Record/playback 2) Keyword/data-driven scripting, 3) Writing Code Using the Programming Language.