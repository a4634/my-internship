# Day 14

**Day 14 - 8th** **April.**

On Day 14 Koushik explained memory and data types. He put it short and sweet “Memory becomes key”. He explained the process in a computer where the input is given, the memory is allotted, the process happens and then after the process output is given. He also explained the different data types with suitable examples which gave a clear picture of data types.

Input —- Memory and process —- Output

Primitive data types: 

There are different primitive data types: string, number, bigint, boolean, undefined, symbol, null, etc. Most of the time, a primitive value is represented directly at the lowest level of the language implementation. All primitives are immutable, i.e., they cannot be altered. It is important not to confuse a primitive itself with a variable assigned a primitive value. The variable may be reassigned. A new value, but the existing value can not be changed in the ways that objects, arrays, and functions can be altered.

Numeric primitives: short, int, long, float and double. These primitive data types hold only numeric data. Operations associated with such data types are those of simple arithmetic (addition, subtraction, etc.) or of comparisons (is greater than, is equal to, etc.)
Textual primitives: byte and char. These primitive data types hold characters (that can be Unicode alphabets or even numbers). Operations associated with such types are those of textual manipulation (comparing two words, joining characters to make words, etc.). However, byte and char can also support arithmetic operations.
Boolean and null primitives: boolean and null.

Composite data types

Composite data types are a combination of primitives and other data types. They include arrays, lists, and collections. Composite data types include arrays, lists, and collections. Combination of primitives and other data types

- Arrays
    - ordered arrangement of data
    - each element
        - keyed – implied or declared (eg. Given a specific code)
        - value held in the element can be any data type

Functions or methods

A method is a block of code which only runs when it is called. You can pass data, known as parameters, into a method. Methods are used to perform certain actions, and they are also known as functions.