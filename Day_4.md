# Day 4

**Day 4 - 25th March.**

In the 4th day of internship Gowshik gave a recap on the first three day sessions. He explained the basis of problem solving with examples. 

Problem = Constrain

Most no of constrain, solve it first.

There is always a smaller bit of a problem that can be solved. There must be part that can be given a solution, with which the entire problem can be solved. He also explained the basis of programming 

- Writing
- Reading
- Problem Solving
- Researching

Other part of the day I was learning Figma. Ameen gave a brief introduction to Figma. It is a web-based graphics editing and user interface design app. It is used for UI/ UX. Ameen explained the difference between UI and UX using ketchup bottle design example. Figma can use to do all kinds of graphic design work from wire-framing apps/ websites, designing mobile app interfaces, prototyping designs, crafting posts for Instagram, Facebook and other social media , It can also be used in logo making, digital arts works etc.

Figma is different from other graphics editing tools. The main advantage of Figma is that it can be used from web browser. The user experience in web is way better than any other web based design tools. This means that we get access to our projects and start designing from any computer or platform without having to buy multiple licenses or install software. It’s more than enough for you to learn, experiment, and work on projects.

What is user interface?

User interface (UI) is anything a user may interact with to use a digital product or service. Example Voice control, usage of buttons in keyboard, remote, touch etc. It is the medium though which we interact with the product.

What is user experience?

The experience that a user gets when the user uses a user interface is called user experience. The user experience may be positive, negative or neutral. It is the responsibility of the UX designers to make the User Interface more user friendly so that the user gets a better user experience.

I started using figma. I was given a screenshot of UI and asked to design the same using figma. The operations in Figma were simple. I learnt to create a UI design in figma. I used the various shapes for search bar, icon etc. I also learned to draw Circle progress bar using circle tool[[https://youtu.be/FxHitLynoMg](https://youtu.be/FxHitLynoMg)]. I was able to draw different icons like profile picture, home button in the application interface etc. Grouping of shapes together made the job more easier. I leaned to create prototype. Some of the shortcuts used are given below

- To make shapes use **R** (for Rectangle)
- **L** (for Line)
- **O** (for Oval, or Ellipse)
- **T** (for Text)
- and **F** (for Frame)

We can pan by holding **SPACE** and dragging with our mouse. Hitting **I** will bring up the color picker. **CTRL+SHIFT+4** will show and hide grids.