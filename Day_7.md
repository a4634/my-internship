# Day 7

**Day 7 - 30th March.** 

Day 7 started with a recap on Proportional statement. Kowshik also explained Proportional logic, Variables, connections etc. Proportional statements are true or false statements

**Logical not**

An assertion that a statement fails or denial of a statement is called the negation of the statement. The negation of a statement is generally formed by introducing the word “not” at some proper place in the statement or by prefixing the statement with “It is not the case that” or It is false that”.
The negation of a statement p in symbolic form is written as “~ p”.

**Logical and**

If two simple statements p and q are connected by the word ‘and’, then the resulting compound statement “p and q” is called a conjunction of p and q and is written in symbolic form as “p ∧ q”.

p : Dinesh is a boy.
q : Nagma is a girl.
The conjunction of the statement p and q is given by
p ∧ q : Dinesh is a boy and Nagma is a girl.

**Logical or**

If two simple statements p and q are connected by the word ‘or’, then the resulting compound statement “p or q” is called disjunction of p and q and is written in symbolic form as “p ∨ q”.

The disjunction of the statements p and q is given by
p : The sun shines.
q : It rains.

p ∨ q : The sun shines or it rains

Various combinations proposition related to other proposition

Influence of one statement’s truth over the other statement’s truth

Statements that are always, sometimes, never true.

**Truth Table**

p, q

p: The house is painted red

q: The house is in the corner of the street

**Logical And ^**

p  q  p^q

T  T   T

T  F  F

F  T  F

F  F  F

**Logical OR v**

p  q  pvq

T  T   T

T  F  T

F  T  F

F  F  F

**Logical Not~**

p ~p

T  F

F  T

**Logical Implication**

If you study well, you will get good marks

p: You study well

q: You will get good marks

p  q  p—>q

T  T   T

T  F  F

F  T  T

F  F  T

**Logical Biconditional**

If and only if you study well, you will get good marks

p: You study well

q: You will get good marks

p  q  p<—>

T  T   T

T  F  F

F  T  F

F  F  T

HTML

Span 

The <span> HTML element is a generic inline container for phrasing content, which does not inherently represent anything. Using span we were able to make a rectangle border into a circular border by changing the properties.

I was given a task by Kingston to find real time examples for bugs in software which brings in software testing as a main requirement.

**Software Testing Examples**

- In April 2015, Bloomberg terminal in London crashed due to software glitch affected more than 300,000 traders on financial markets. It forced the government to postpone a 3bn pound debt sale.
- Nissan cars recalled over 1 million cars from the market due to software failure in the airbag sensory detectors. There has been reported two accident due to this software failure.
- Starbucks was forced to close about 60 percent of stores in the U.S and Canada due to software failure in its POS system. At one point, the store served coffee for free as they were unable to process the transaction.