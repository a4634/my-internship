# Day 5

**Day 5 - 26th March.**

The 5th day of the internship started with Kowshik’s session on problem solving. He explained the Konigsberg Bridge Problem, where in he explained the different possibilities for a solution to problem. He used this example to show that there are problems without desired solutions. 

**F + V = E + 2**
where F is the number of faces, V the number of vertices, and E the number of edges. A cube, for example, has 6 faces, 8 vertices, and 12 edges and satisfies this formula.

**Markdown**

Markdown (.md) is a file format in which text formatting can be done. For this we need to create a .md file.

Create .md file. You get access to txt formatting 

Vim <file name>.md

Consider the file name is ah.md

You can edit the ah.md file in Gitlab itself. Click on ah.md, you will find an edit button. You can preview your changes and even commit them from there. The major advantage is that when we apply the specific commands like # before a text, when opened in gitlab the letters after the # are formatted to heading format. This is the major advantage [of](http://of.md) .md file. The same can be done from gitlab too. There are various 

Headings

To create a heading, add number signs (#) in front of a word or phrase. The number of number signs you use should correspond to the heading level.

Similarly <p> is for paragraph formatting. To create a line break or new line (<br>), end a line with two or more spaces, and then type return. 

Bold: To bold text, add two asterisks or underscores before and after a word or phrase. To bold the middle of a word for emphasis, add two asterisks without spaces around the letters.

Italic: To italicize text, add one asterisk or underscore before and after a word or phrase. To italicize the middle of a word for emphasis, add one asterisk without spaces around the letters.

Ordered list: To create an ordered list, add line items with numbers followed by periods. The numbers don’t have to be in numerical order, but the list should start with the number one.

To create an unordered list, add dashes (`-`), asterisks (`*`), or plus signs (`+`) in front of line items. Indent one or more items to create a nested list.

Horizontal line: To create a horizontal rule, use three or more asterisks (`***`), dashes (`---`), or underscores (`___`) on a line by themselves.

**Figma**

I started working on wire frames. I created a rough wire frame on a paper through hand sketch. A wireframe is a rough sketch of how you want to fully realize your product in terms of a visual layout. ****Ameen explained what wire frames are with examples from [dribbble.com](http://dribbble.com). ****You will get a better clarity on the output of the product in the initial stage. It is all bout the position of buttons, objects in the application and initial rough draft or plan or rough sketch of the application. This is used to showcase the basic User Interface. I started working on Figma to create the wire frame for my application. Figma is a beautiful application to create User Interface models.