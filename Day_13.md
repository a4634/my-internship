# Day 13

**Day 13 - 7th** **April.**

On the thirteenth day of the internship, during the afternoon session, Kowshik started explaining lexical analysis, syntax analysis, semantic analysis, compilation, compiler, Unicode etc. He explained all these referring to examples from the English language.

Lexical analysis: **Lexical Analysis** is the very first phase in the compiler design. A Lexer takes the modified source code which is written in the form of sentences. In other words, it helps you to convert a sequence of characters into a sequence of tokens. The lexical analyzer breaks this syntax into a series of tokens. It removes any extra space or comment written in the source code. Programs that perform Lexical Analysis in compiler design are called lexical analyzers or lexers.

Syntax analysis: **Syntax Analysis** is a second phase of the compiler design process in which the given input string is checked for the confirmation of the rules and structure of the formal grammar. It analyses the syntactical structure and checks if the given input is in the correct syntax of the programming language or not. Syntax Analysis in the Compiler Design process comes after the Lexical analysis phase.

- Check if the code is valid grammatically
- The syntactical analyzer helps you to apply rules to the code
- Helps you to make sure that each opening brace has a corresponding closing balance
- Each declaration has a type and the type must be exists

Semantic analysis:  **Semantic Analysis** is the third phase of **[Compiler](https://www.geeksforgeeks.org/introduction-compiler-design/)**. Semantic Analysis makes sure that declarations and statements of the program are semantically correct. It is a collection of procedures that are called by the parser as and when required by the grammar. Both syntax trees of the previous phase and symbol tables are used to check the consistency of the given code. **Type checking** is an important part of the semantic analysis where the compiler makes sure that each operator has matching operands.

**Semantic Analyzer:** It uses a syntax tree and symbol table to check whether the given program is semantically consistent with the language definition. It gathers type information and stores it in either a syntax tree or symbol table. This type of information is subsequently used by the compiler during intermediate-code generation.

Compilation: Compilation is the process the computer takes to convert a high-level programming language into a machine language that the computer can understand.

Compiler: A compiler is a special program that processes statements written in a particular programming language and turns them into machine language or "code" that a computer's processor uses.