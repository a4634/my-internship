# Day 12

**Day 12 - 6th** **April.**

On the 12th day of the internship, Ameen corrected my solutions for Algorithm problems. He gave me more insights to make the computer understand the problem and solve it. Then we worked on writing codes in Flutter. He corrected the codes that I have written for the example Application pages that were given to me as homework.

Ameen explained to me some of the basic widgets. 

Text

The Text widget lets you create a run of styled text within your application.

Row, Column can contain children.

These flex widgets let you create flexible layouts in both the horizontal (`Row`) and vertical (`Column`) directions. Rows and columns 

Container 

The container is a rectangular box, which can contain only one child.

- The first letter of the widget is always capital
- The things that change the properties of the widgets are written in small letters.
- Widgets use ()
- children use []

The second session was taken by Abraham. He started with Data. The session was interesting as he interacted more with the audience. The only language that the computer knows is binary language i.e., 0 and 1. The best example is switch 1 is ON and 0 is OFF

ASCII - **American Standard Code for Information Interchange**

Ascii is a standard data-transmission code that is used by smaller and less-powerful computers to represent both textual data (letters, numbers, and punctuation marks) and noninput-device commands (control characters).

UTF stands for "**UCS (Unicode) Transformation Format**". It is the default character set for HTML5. Over 95% of all websites, store characters this way.

In computing, data is information that has been translated into a form that is efficient for movement or processing. Relative to today's computers and transmission media, data is information converted into binary digital form. 

What is a pixel?

For example, a computer with a display resolution of 1280 x 768 will produce a maximum of 9,83,040 pixels on a display screen.

2.1 megapixels picture contains 2,073,600 pixels since it has a resolution of 1920 x 1080.

In 8-bit color systems, only one byte is allocated per pixel, limiting the palette to just 256 colors. In the common 24-bit color systems used for nearly all PC monitors and smartphone displays, three bytes are allocated, one for each color of the RGB scale, leading to a total of 16,777,216 color variations. A 30-bit deep color system allocates 10 bits each of red, green, and blue for a total of 1.073 billion color variations. However, since the human eye cannot discriminate more than ten million colors, more color variations do not necessarily add more detail, and may even lead to color banding issues.

Frames per second

The speed at which those images are shown, or how fast you “flip” through the book. It's usually expressed as “frames per second,” or FPS. So if a video is captured and played back at 24fps, that means each second of the video shows 24 distinct still images.