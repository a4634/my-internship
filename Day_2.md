# Day 2

**Day 2 - 23rd March.** 

Software Testing: Software testing is the process of finding errors in the developed product. It also checks whether the real outcomes can match expected results, as well as aids in the identification of defects, missing requirements, or gaps. I learnt this from Kingston. There was a correct set of codes with correct details which includes the phone number, fax number, address, company code etc. I helped him find the possible errors that can be made while entering company code and phone number. For example, there are set of 10 digit numbers in phone number, if 11 digits are entered, then it is an error and it is one of the possible error outcome. Similarly if less than 10 digits are entered, then it is an error and it is one of the possible error outcome. I helped Kingston find 4 to 5 possible errors that may arise. This helped me gain knowledge on testing. 

Next, I was working on the console trying different commands. Ameen gave me different set of commands to get desired outcome. For example **ls -** It lists the content of a given directory. cd command, which stands for Change Directory, **rmdir** To delete empty directory, **rm -r** To delete a directory with file, **mkdir** Make new directory, **touch -** Create new file. Ameen referred a link [https://linuxize.com/post/how-to-remove-files-and-directories-using-linux-command-line/](https://linuxize.com/post/how-to-remove-files-and-directories-using-linux-command-line/) for me try and understand different commands. Then he gave me tasks to move files from one directory to another using mv command. Also to delete files, directories, directories with files etc. This helped me understand the different operation that we do using GUI and the commands. He helped me by opening the file manager and showing the changes we commands are given in the console. When we give a command mkdir with name AH, a new folder is created in file manager with the name AH.

During the second session, Kowshik explained what a problem is and the steps to solve any problem 

To solve a problem

Step 1 Understand the problem

Step 2 Writing down the constrain

Step 3 Write down the possible operations

Step 4 Arrange the operation , satisfying the constraints (possible solution )

The problem is generally constraint . 

During the session we were taught to use vim. Vim is a text editor.

vim file name with txt  opens a vim file in txt format. The same file can be opened in kwrite. This done by giving the command kwrite file name. To save :wq command is used.The common commands used are given below

 Insert mode (Where you can just type like normal text editor. Press i for insert mode)

Command mode (Where you give commands to the editor to get things done . Press ESC for command mode)

Most of them below are in command mode

- x - to delete the unwanted character
- u - to undo the last the command and U to undo the whole line
- CTRL-R to redo
- A - to append text at the end
- :wq - to save and exit
- :q! - to trash all changes
- dw - move the cursor to the beginning of the word to delete that word
- 2w - to move the cursor two words forward.
- 3e - to move the cursor to the end of the third word forward.
- 0 (zero) to move to the start of the line.
- d2w - which deletes 2 words .. number can be changed for deleting the number of consecutive words like d3w
- dd to delete the line and 2dd to delete to line .number can be changed for deleting the number of consecutive words