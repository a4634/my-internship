# Day 3

**Day 3 - 24th March.** 

Software Testing: Software testing is classified into two main categories. 

- Functional Testing
- Non-functional testing

Functional Testing: Functional testing involves the testing of the functional aspects of a software application. When you’re performing functional tests, you have to test each and every functionality. You need to see whether you’re getting the desired results or not.

Non- Functional testing: Non-functional testing is the testing of non-functional aspects of an application, such as performance, reliability, usability, security, and so on. Non-functional tests are performed after the functional tests.

**Black Box Test Technique:** In the black-box testing, major testing will be around possible inputs and expected outputs. A tester should be able to choose the valid test data carefully. In simple terms, a tester can only see the actions of the AUT. The tester need not know how those actions are performed.

**White-box test technique:** White box testing is a test approach that is used to test the implementation part of an application under test. To perform this testing, the tester/possibly the developer should know the internal structure of the application and how it works. 

Example: **CAR** is the **AUT (Application Under Test).**

The **user** is the **black box tester.** The **mechanic** is the **white box tester.**

Next, Ameen explained me what UI and UX is. He also explained how applications are made from basics. He gave me outline of app development. He explained the basics and uses of Flutter.

Flutter: Flutter is an open source framework to create high quality, high performance mobile applications across mobile operating systems - Android and iOS. It provides a simple, powerful, efficient and easy to understand SDK to write mobile application in Google’s own language, *Dart.* 

**SDK**: A software development toolkit (SDK) is a set of software tools and programs provided by hardware and software vendors that developers can use to build applications for specific platforms.

Single code base to develop applications for iOS, Android, Windows, Mac, Linux 

During the second session, Kowshik explained how a number swapping puzzle is solved 

The problem is generally constraint . He referred to a sudoku example and showed that the problem with more constrains are solved first.

During the session we were taught to install git. We created a new account in git lab. 

What is version control system? 

Version control refers to the practice of tracking and managing changes to software code. 

Every project under the distributed version control system Git, goes through three stages — **Modified, Staged, and Committed.**

Reference: [https://archaeogeek.github.io/foss4gukdontbeafraid/git/stages.html](https://archaeogeek.github.io/foss4gukdontbeafraid/git/stages.html)

The most commonly used git commands are:
add - Add file contents to the index
bisect - Find by binary search the change that introduced a bug
branch - List, create, or delete branches
checkout - Checkout a branch or paths to the working tree
clone - Clone a repository into a new directory
commit - Record changes to the repository
diff - Show changes between commits, commit and working tree, etc
fetch - Download objects and refs from another repository
grep - Print lines matching a pattern
init - Create an empty Git repository or reinitialize an existing one
log - show commit logs
merge - Join two or more development histories together
mv - Move or rename a file, a directory, or a symlink
pull - Fetch from and merge with another repository or a local branch
push - Update remote reps along with associated objects
rm - Remove files from working tree and from index