# Day 15

**Day 15 - 11th** **April.**

On the 15th day of the internship, Askin started the afternoon session with the topic of Array. He explained Array with different examples (eg. group). The array may contain numbers, variables strings etc. He pointed out that the array does not know whether the data entered string or number or anything. The memory allocation in the array starts from 0,1,2,3,... He also explained and revised what Kowshik taught us regarding compiler, lexical analyzer, semantic analyzer etc. He gave us homework to find the working function of the following functions in Javascript.

**Push**

Array.prototype.push()
Adds one or more elements to the end of an array, and returns the new length of the array.

**Pop**

Array.prototype.pop()
Removes the last element from an array and returns that element.

**Slice**

Array.prototype.slice()

Extracts a section of the calling array and returns a new array.

**Splice**

Array.prototype.splice()
The splice() method changes the contents of an array by removing or replacing existing elements and/or adding new elements in place. To access part of an array without modifying it, see slice().

**Unshift**

Array.prototype.unshift()
Adds one or more elements to the front of an array, and returns the new length of the array.

**Shift**

Array.prototype.shift()
Removes the first element from an array and returns that element.

**Filter**

Array.prototype.filter()
The filter() method creates a new array with all elements that pass the test implemented by the provided function.

**Map**

Array.prototype.map()
Returns a new array containing the results of invoking a function on every element in the calling array.

**Find**

Array.prototype.find()
Returns the found element in the calling array, if some element in the array satisfies the testing function, or undefined if not found.

**Reduce**

Array.prototype.reduce()
Executes a user-supplied "reducer" callback function on each element of the array (from left to right), to reduce it to a single value.

Different ways function is written on javascript

- Function Declaration.
- Function Expression.
- Arrow Function Expression.
- Concise Arrow Function Expression.

Askin also explained what an object is and he explained it with humans as an example. Humans have different properties in key-value pairs

Object: Object is the collection of properties in key-value pairs. The array has square brackets 

whereas the object has flower brackets.

Then he explained the difference between Friendship and Being friendly. He listed the difference between Friendship and being friendly. He reminded the example used by Abraham Anna to explain friendliness. He compared the sharpness of the knife to the skill set we have developed and the Knife handle is compared to being friendly. He gave a clear meaning for being friendly and it made sense. He also told that being approachable is one of the main aspects of being friendly. He gave a good picture explaining the need for being friendly in the workspace. He also told that a good friendship only happens when one or the other corrects the person who is doing a mistake or going on the wrong path. It also created a great impact in my mind and heart.