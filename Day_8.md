# Day 8

**Day 8 - 31st March.** 

I was given a task by Gayathri to know what testing is and the importance of it.

What is Software Testing?

Software testing is to check whether the software is meeting the expected requirements. It also helps to ensue that the software is defect free. The purpose of software testing is to identify errors, gaps or missing requirements. Software Testing means the Verification of Application Under Test (AUT).

**Importance of Software testing**

A well tested software is expect to be reliable, secure and high efficient.  It also helps in time saving, cost effectiveness and customer satisfaction. Testing is important because software bugs could be expensive or even dangerous.

Typically Testing is classified into three categories.

- Functional Testing
- Non-Functional Testing or Performance testing
- Maintenance (Regression and Maintenance)

I was given a task by Ameen do a presentation on the difference between UI & UX. I was explained the difference between UI & UX. The various phases undergone in UX design was explained.

The usage of inline CSS in HTML was explained by Rexilia. CSS stands for Cascading Style Sheets.  CSS saves a lot of work. It can control the layout of multiple web pages all at once. Cascading Style Sheets (CSS) is used to format the layout of a webpage. With CSS, you can control the color, font, the size of text, the spacing between elements, how elements are positioned and laid out, what background images or background colors are to be used, different displays for different devices and screen sizes. An inline CSS is used to apply a unique style to a single HTML element. An inline CSS uses the style attribute of an HTML element.

The second session was started by Askin, with a small introduction on Algorithms 

In computer programming terms, an algorithm is a set of well-defined instructions to solve a particular problem. It takes a set of input and produces a desired output.

Home work was given by Askin to practice algorithms, how to break up a task into smaller algorithms so that the task can be finished easier.

Fight scene

To A & B

1. Get down on all fours, 
2. placing your hands slightly wider than your shoulders.
3. Straighten your arms and legs.
4. Lower your body until your chest nearly touches the floor.
5. Pause, then push yourself back up.
6. Repeat for 5 times.

A and B stand up

A swing your hand and hit B on face

B reacts with a sound aaa

B hits back A

C move forward 

Hold the hands of A and B

Command Stop the fight

A and B stand facing each other

Shake hands

Task to make biriyani

- Take 1 cup boiled basmati rice
- Take 600 gm chicken
- Take 1/2 teaspoon mint leaves
- Take 1 tablespoon garam masala powder
- Take salt as required
- Take 1 teaspoon saffron
- Take 2 tablespoon refined oil
- Take 1 tablespoon bay leaf
- Take 3 green cardamom
- Take 1 black cardamom
- Take 2 clove
- Take 1 teaspoon cumin seeds
- Take 2 onion
- Take 4 green chillies
- Take 1 teaspoon turmeric
- Take 1 tablespoon ginger paste
- Take 1 tablespoon garlic paste
- Take 1 teaspoon red chilli powder
- Take 1 cup hung curd
- Take 1/2 tablespoon ginger
- Take 2 tablespoon coriander leaves
- Take 2 drops kewra
- Take water as required
- Take 1 tablespoon rose water
- 1 tablespoon ghee