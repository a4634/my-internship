# Day 9

**Day 8 - 1st April.** 

I was given a task by Gayathri to Know the Difference between a Test Plan and a Test strategy. A test plan is a  project-level document that contains all the testing activities to be done to deliver a quality product. It is very specific to the project and takes key information from the Test Strategy document. It is a dynamic document and it must be kept up to date. It is one of the documents in the test deliverables. It is not specific to a single project.

Test strategy is an organizational level document that contains general details of testing. Test strategy might be included in the Test Plan if it is a smaller project. It is a constant document and can be altered if the project requires it.

The second session was started by Rexelia. She explained what external CSS is. We tried using external CSS in webpage design.

The session was continued by a presentation on networking. Haridev explained the basics of networking. 

A computer network comprises two or more computers that are connected—either by cables (wired) or WiFi (wireless)—to transmit, exchange, or share data and resources. You build a computer network using hardware (e.g., routers, switches, access points, and cables) and software (e.g., operating systems or business applications).

Geographic location often defines a computer network. For example, a LAN (local area network) connects computers in a defined physical space, like an office building, whereas a WAN (wide area network) can connect computers across continents. The internet is the largest example of a WAN, connecting billions of computers worldwide.

You can further define a computer network by the protocols it uses to communicate, the physical arrangement of its components, how it controls traffic, and its purpose.

In computer programming terms, an algorithm is a set of well-defined instructions to solve a particular problem. It takes a set of inputs and produces the desired output.

In the evening session, Askin explained their obsession with different examples. He also made us understand the need for obsession with positive things which make us grow. The people we know who are at the top of their game got there not by wanting to be better, but by becoming obsessed with improving. Because we live in such a result-oriented society, it's easy to forget that the key to happiness lies not in the end goal, but in the efforts along the way toward achieving the goals we've set for ourselves.

Later Anoop explained, how a presentation should be and he also shared his first presentation experience. He explained the 1:2:3 ratio rule in the presentation. If a presentation is having 10 slides, the presentation time for the presenter can be 20 min and the questionnaire session after the presenter completes the presentation maybe 30 min. This must be kept in mind while making a presentation.