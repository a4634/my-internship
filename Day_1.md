# Day 1

**Day 1 22nd March 2022**

The major learnings of this day are comprised in this essay. The day started with a small introduction speech from Abraham the CTO & Co-Founder of Surfboard Payments India Private Limited. Reading, Writing, and communicating are the main skills learned from schools. The same is required in the field of Coding to communicate with the computer. So it is always important  to improve the communication skill first. The difference between skill and knowledge was explained. For example, BE is more focused on knowledge whereas B.Tech is more focused on skills. To improve my communication skills it was suggested to write 500 words a day. It was stressed by Kaushik the CEO of Core Illustrio that the knowledge gained during the program must be recorded and not what is happening during the day. The other assignment provided was to write a 5000 words summary of the book Extreme Ownership: How U.S Navy SEALs Lead and Win, Jocko Willink and Leif Babin. It was insisted by Askin to buy this book and start reading it. Kowshik introduced the mentors. My mentor was Ameen. It was suggested to learn from the mentors and also to complete the tasks given by the mentor to myself. My mentor Ameen gave me a task to find all the names, their role in the company and also their favourite movie so that I would get introduced to the software engineer team of Surfboard payment. The other task given by the mentor was to find the different OS used in computers, the function of OS.

Kowshik started the second half session with the quote “be comfortable being uncomfortable”. It means that you're getting used to being in situations that are outside of your comfort zone. You might even actively pursue activities that put you outside of your comfort zone. He also explained the various skill graphs vertical, horizontal, t shaped etc. A T-shaped individual possesses excellent knowledge of and skills in specific areas and is good at collaboratively working with others. T-shaped employees excel in their main responsibilities but they can also perform other tasks effectively. The I-shaped individual possesses a deep understanding of and skills in a given arena. However, they have not tried or have not been successful at applying said knowledge or skills to other areas. V-Shaped people are different to T-Shaped people is that, as they grow in knowledge and skill, they grow in spiral fashion both horizontally and vertically. V-Shaped people are the kind that we need for the New Way of Working – so, focus on careers and competency development both across and up and grow your tomorrow’s workforce. He came up with different skills and clearly explained the difference between hard skills and soft skills. soft skills are interpersonal and behavioural skills that help you work well with other people. Communication, Problem-solving, Creativity, Adaptability, Work ethic, etc. are some of the soft skills. Hard skills are abilities you learn in school or on the job. They're things like C# programming, marketing campaign management, and financial forecasting. He also explained what problem-solving is. He explained with example the difference between problem solvers (solutions) and problem avoiders. It better to face the problem than to avoid them.

**Six step guide to help you solve problems**

Step 1: Identify and define the problem

Step 2: Generate possible solutions

Step 3: Evaluate alternatives

Step 4: Decide on a solution

Step 5: Implement the solution

Step 6: Evaluate the outcome

**What Is an Operating System (OS)?**

An operating system is a software programme required to manage and operate a computing device like smartphones, tablets, computers, supercomputers, web servers, cars, network towers, smartwatches, etc. It is the operating system that eliminates the need to know coding language to interact with computing devices. It is a layer of graphical user interface (GUI), which acts as a platform between the user and the computer hardware. Moreover, the operating system manages the software side of a computer and controls programs execution.

The dominant general-purpose personal computer operating system is Microsoft Windows with a market share of around 76.45%. macOS by Apple Inc. is in second place (17.72%), and the varieties of Linux are collectively in third place (1.73%).

**What Is the Function of an Operating System?**

The functions of OS [i](https://en.wikipedia.org/wiki/Operating_system)nclude:

- Booting: Booting is the process of turning on the computer and powering up the system.
- Memory management: This feature controls and coordinates the computer applications while allocating space for programs.
- Loading and execution: Your OS will load, or start up, a program and then execute the program so that it opens and runs.
- Data security: A good OS includes features that keep your data safe and computer programs secure. Security features are set up to keep unwanted cyberattackers at bay.
- Disk management: This manages all the drives installed in a computer, including hard drives, optical disk drives, and flash drives. Disk management can also be used to divide disks, format drives, and more.
- Process management: Your OS is designed to allocate resources to different computer processes, enable the processes to share information, protect them, and synchronize them.
- Device controlling: Your OS will allow you to open or block access to devices like removable devices, CD/DVDs, data transfer devices, USBs, and more.
- Printing controlling: As an extension of device controlling, your OS takes control of the printers that are connected to the computer, and the materials that need to be printed.
- User interface: Also referred to as a UI, this is the part of the OS that allows a user to enter and receive information. This can be done with typed commands, code, and other formats.

**LINUX**

Linux® is an open source operating system (OS). Linux is normally used in combination with the GNU operating system: the whole system is basically GNU with Linux added, or GNU/Linux. All the so-called “Linux” distributions are really distributions of GNU/Linux.

The Linux command line is a text interface to your computer. It's where you ask software to perform hardware actions that point-and-click graphical user interfaces (GUIs) simply can't ask.

**Basic commands**

**pwd**

As we have seen before, `pwd` will simply print out the current working directory.

**ls**

It lists the content of a given directory. The peculiarity of this command is that it supports a wide range of arguments.

**file**

linux does not rely on file extension to understand what kind of file it is dealing with. file command outputs the real file kind.

**cat**

The `cat` command will print out the content of a text file given as argument.

**cd**

The `cd` command, which stands for Change Directory, will change your working directory to the one passed as argument.

**clear**

After some time using the terminal, your screen will look messy and confusing.To clear the screen and start all over, type the `clear` command.

**history**

The `history` command, will show an historical list of commands that were entered in the terminal session.

**cp**

The `cp` command, which stands for copy, is used to create a copy of a file/directory.The `cp` command will not produce any output, but our action was performed as expected.

**mv**

The `mv` command, which stands for move, moves a file/folder to a new location, or renames it. To rename the file `copy_of_john_file` to `john_file_renamed` we will use the `mv`.To move the file `john_file_renamed`inside `john_directory` we will still use `mv`.

**Note:** When specifying a file in the terminal, a trailing forward-slash “/” indicates it is a directory.

Like `cp`, the `mv` command will not produce any output even though our operation was performed successfully.

**rm**

The  `rm`, which stands for remove. It is used to delete files, but can delete directories as well if instructed to do so. The `rm`command like the previous commands will normally not produce any output unless an error is generated. The arguments are `-r` (recursive) and `-f`(force). The first one induces the operation to work on every single file and folder contained recursively on the directory(ies) passed as argument(s), while the latter forces the operation ignoring errors and disabling any confirmation prompt.

 **rmdir**

To delete empty directory 

**rm -r** 

To delete a directory with file

**mkdir**

Make new directory

**touch**

Create new file

**CLI**

CLI stands for **Command Line Interface**. It is an interface for the user that is used to issue commands in successive lines of text or command lines to execute the tasks. It is a platform or medium in which users react to a visible prompt by writing a command and receiving a response from the system, and the users have to be compelled to type command or train of command to execute the task. It is suitable for high-priced computing where input accuracy is critical.

UNIX's operating systems include a CLI, whereas Windows and Linux include both a CLI and a GUI. The user must have good knowledge of using the CLI and complete knowledge of the correct syntax to issue effective commands. Overall, the CLI uses less memory and executes faster than the GUI.

**GUI**

GUI stands for **Graphical User Interface**. A GUI enables users to interact with the operating system or application. GUI offers buttons, windows, scrollbars, iconic images, wizards, and other icons to facilitate users. It has a user-friendly interface for beginners. It is easy to use, learn and also reduces the cognitive load. Unlike CLI users, GUI users do not need to remember commands; rather, they have to be able to recognize them and perform good exploratory analysis and graphics.

**Cent OS**

CentOS is a Linux distribution that provides a free and open-source community-supported computing platform, functionally compatible with its upstream source, Red Hat Enterprise Linux.

**KDE Plasma Workspace vs GNOME**

GNOME and KDE applications share general task related capabilities, but they also have some design differences. KDE applications for example, tend to have more robust functionality than GNOME.